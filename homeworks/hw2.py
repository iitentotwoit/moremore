class Counter:
    def __init__(self, initial_value):
        self.value = initial_value

    def inc(self):
        self.value += 1
        return self.value

    def dec(self):
        self.value -= 1
        return self.value


class ReverseCounter(Counter):
    def inc(self):
        self.value -= 1
        return self.value

    def dec(self):
        self.value += 1
        return self.value


def get_counter(number):
    if number < 0:
        return ReverseCounter(number)
    return Counter(number)


counter_1 = get_counter(5)
a = counter_1.inc()
b = counter_1.inc()
c = counter_1.inc()
d = counter_1.dec()

print(a, b, c, d)

counter_2 = get_counter(-1)
a = counter_2.inc()
b = counter_2.inc()
c = counter_2.inc()
d = counter_2.dec()

print(a, b, c, d)
