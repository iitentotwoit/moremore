# 1
def get_date(day, month, year):
    """Задание №1"""
    months = [
        "января",
        "февраля",
        "марта",
        "апреля",
        "мая",
        "июня",
        "июля",
        "августа",
        "сентября",
        "октября",
        "ноября",
        "декабря",
    ]
    return f"{day} {months[month - 1]} {year} года"


print(get_date(11, 12, 2023))


# 2
def count_names(names):
    """Задание №2"""
    return {i: names.count(i) for i in names}


names = ("Олег", "Игорь", "Анна", "Олег", "Анна", "Анна")
print(count_names(names))


# 3
def name_get(dictionary):
    """Задание №3"""
    dict_keys = [i for i in dictionary.keys()]
    dict_values = [i for i in dictionary.values()]

    if len(dict_keys) == 3:
        return f"{dict_values[0]} {dict_values[1]} {dict_values[2]}"
    elif len(dict_keys) == 2:
        if "first_name" not in dict_keys:
            return f'{dictionary["last_name"]}'
        else:
            return f"{dict_values[0]} {dict_values[1]}"
    elif len(dict_keys) == 1 and "middle_name" in dict_keys:
        return f"Нет Данных"
    else:
        return f"Нет Данных"


name = {
    "first_name": "Иван",
    "last_name": "Иванов",
    "middle_name": "Иванович",
}  # Полное ФИО
name1 = {"first_name": "Иван", "last_name": "Иванов"}  # Отсутствует отчество
name2 = {"last_name": "Иванов", "middle_name": "Иванович"}  # Отсутствует имя
name3 = {"first_name": "Иван", "middle_name": "Иванович"}  # Отсутствует фамилия
name4 = {"middle_name": "Иванович"}  # Только отчество
name5 = {}  # Нет данных

all_name = [name, name1, name2, name3, name4, name5]
for name in all_name:
    print(name_get(name))


# 4
def is_prime(number):
    """Задание №4"""
    for i in range(2, (number // 2) + 1):
        if number % i == 0:
            return False
    return True


print(is_prime(7))


# 5
def unique_numbers(*args):
    """Задание №5"""
    unique_list = []
    for i in args:
        if isinstance(i, int):
            unique_list.append(i)
    unique_list.sort()
    return unique_list


a = None, "a", 6, 73, "f", 52, "14", 16
print(unique_numbers(*a))
