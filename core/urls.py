from core.views.views import Homepage, DetailedPage, CreateProduct, EditProduct
from core.views.men import (
    CategoriesMen,
    TopsMen,
    BottomsMen,
    OuterwearMen,
    FootwearMen,
    AllMen,
)
from core.views.women import (
    CategoriesWomen,
    TopsWomen,
    BottomsWomen,
    OuterwearWomen,
    FootwearWomen,
    AllWomen,
)
from django.urls import path

urlpatterns = [
    path("", Homepage.as_view(), name="homepage"),
    path("men/", CategoriesMen.as_view(), name="men"),
    path("men/all_clothes", AllMen.as_view(), name="all_men"),
    path("men/tops", TopsMen.as_view(), name="men_tops"),
    path("men/bottoms", BottomsMen.as_view(), name="men_bottoms"),
    path("men/outerwear", OuterwearMen.as_view(), name="men_outerwear"),
    path("men/footwear", FootwearMen.as_view(), name="men_footwear"),
    path("women/", CategoriesWomen.as_view(), name="women"),
    path("women/tops", TopsWomen.as_view(), name="women_tops"),
    path("women/all_clothes", AllWomen.as_view(), name="all_women"),
    path("women/bottoms", BottomsWomen.as_view(), name="women_bottoms"),
    path("women/outerwear", OuterwearWomen.as_view(), name="women_outerwear"),
    path("women/footwear", FootwearWomen.as_view(), name="women_footwear"),
    path("<int:product_id>", DetailedPage.as_view(), name="detailed_page"),
    path("create_product", CreateProduct.as_view(), name="create_product"),
    path("<int:product_id>/edit_product", EditProduct.post_edit, name="edit_product"),
]
