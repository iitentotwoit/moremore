from django import forms
from core import models


class Product(forms.ModelForm):
    class Meta:
        model = models.Product
        fields = "__all__"
