from django.db import models


class Collection(models.Model):
    """Коллекция одежды"""

    MEN = "MEN"
    WOMEN = "WOMEN"
    GENDER_CHOICES = [(MEN, MEN), (WOMEN, WOMEN)]

    class Meta:
        verbose_name = "Коллекция"
        verbose_name_plural = "Коллекции"

    name = models.CharField(
        verbose_name="Название коллекции",
        max_length=120,
    )
    gender = models.CharField(
        verbose_name="Пол",
        max_length=5,
        choices=GENDER_CHOICES,
    )
    created = models.DateTimeField(
        verbose_name="Дата создания",
        auto_now_add=True,
    )

    def __str__(self):
        return f"{self.gender}'s {self.name}"


class ProductCategory(models.Model):
    """Категории"""

    TOPS = "TOPS"
    BOTTOMS = "BOTTOMS"
    OUTERWEAR = "OUTERWEAR"
    FOOTWEAR = "FOOTWEAR"

    CATEGORY_CHOICES = [
        (TOPS, TOPS),
        (BOTTOMS, BOTTOMS),
        (OUTERWEAR, OUTERWEAR),
        (FOOTWEAR, FOOTWEAR),
    ]

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    collection = models.ForeignKey(
        Collection,
        on_delete=models.PROTECT,
        related_name="categories",
    )
    category = models.CharField(
        verbose_name="Категория",
        max_length=9,
        choices=CATEGORY_CHOICES,
    )
    created = models.DateTimeField(
        verbose_name="Дата создания",
        auto_now_add=True,
    )

    def __str__(self):
        return f"{self.category} - {self.collection}"


class Product(models.Model):
    """Товар"""

    XS = "XS"
    S = "S"
    M = "M"
    L = "L"
    XL = "XL"
    XXL = "XXL"
    ONESIZE = "ONESIZE"

    SIZE_CHOICES = [
        (XS, XS),
        (S, S),
        (M, M),
        (L, L),
        (XL, XL),
        (XXL, XXL),
        (ONESIZE, ONESIZE),
    ]

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"

    collection_name = models.ForeignKey(
        Collection,
        on_delete=models.PROTECT,
        verbose_name="Название коллекции",
        related_name="products",
    )
    category = models.ForeignKey(
        ProductCategory,
        on_delete=models.PROTECT,
        verbose_name="Категория",
        related_name="product",
    )
    name = models.CharField(
        verbose_name="Название товара",
        max_length=120,
    )
    color = models.CharField(
        verbose_name="Цвет",
        max_length=35,
    )
    material = models.CharField(
        verbose_name="Материал",
        max_length=70,
    )
    size = models.CharField(
        verbose_name="Размер",
        max_length=7,
        choices=SIZE_CHOICES,
        default=ONESIZE,
    )
    price = models.DecimalField(
        verbose_name="Цена",
        max_digits=8,
        decimal_places=2,
    )
    description = models.TextField(
        verbose_name="Описание",
        blank=True,
    )
    is_available = models.BooleanField(
        verbose_name="Наличие",
        default=True,
    )
    quantity = models.PositiveIntegerField(
        verbose_name="Количество",
        default=0,
    )
    image = models.ImageField(
        blank=True,
        upload_to="images",
    )
    created = models.DateTimeField(
        verbose_name="Дата создания",
        auto_now_add=True,
        blank=True,
    )
    modified = models.DateTimeField(
        verbose_name="Дата изменения",
        auto_now=True,
        blank=True,
    )

    def __str__(self):
        return f"{self.name}, {self.category} - {self.collection_name}"
