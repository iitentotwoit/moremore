from django.contrib import admin

from core.models import Collection, ProductCategory, Product


@admin.register(Collection)
class CollectionAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "gender",
        "created",
    )


@admin.register(ProductCategory)
class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "category",
        "created",
    )


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "color",
        "material",
        "size",
        "description",
        "price",
        "is_available",
        "quantity",
        "image",
        "created",
        "modified",
    )
