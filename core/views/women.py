from django.shortcuts import render
from django.views import View
from django.views.generic import ListView
from core.models import Product


class CategoriesWomen(View):
    def get(self, request):
        context = {"title": "WOMEN"}
        return render(
            request=request,
            template_name="core/categories_women.html",
            context=context,
        )


class AllWomen(ListView):
    template_name = "core/catalog_all_women.html"
    extra_context = {"title": "ALL"}
    context_object_name = "category"

    def get_queryset(self):
        queryset = Product.objects.filter(collection_name="2")

        sort_param = self.request.GET.get("sort_by")
        if sort_param == "price_a":
            queryset = queryset.order_by("price")
        elif sort_param == "price_d":
            queryset = queryset.order_by("-price")

        search_name = self.request.GET.get("search_name")
        if search_name:
            queryset = queryset.filter(name__contains=search_name)

        return queryset


class TopsWomen(ListView):
    template_name = "core/catalog.html"
    extra_context = {"title": "TOPS"}
    context_object_name = "category"
    queryset = Product.objects.filter(category="5").filter(collection_name="2")


class BottomsWomen(ListView):
    template_name = "core/catalog.html"
    extra_context = {"title": "BOTTOMS"}
    context_object_name = "category"
    queryset = Product.objects.filter(category="6").filter(collection_name="2")


class OuterwearWomen(ListView):
    template_name = "core/catalog.html"
    extra_context = {"title": "OUTERWEAR"}
    context_object_name = "category"
    queryset = Product.objects.filter(category="7").filter(collection_name="2")


class FootwearWomen(ListView):
    template_name = "core/catalog.html"
    extra_context = {"title": "FOOTWEAR"}
    context_object_name = "category"
    queryset = Product.objects.filter(category="8").filter(collection_name="2")
