from django.shortcuts import render
from django.views import View
from django.views.generic import ListView
from core.models import Product


class CategoriesMen(View):
    def get(self, request):
        context = {"title": "MEN"}
        return render(
            request=request,
            template_name="core/categories_men.html",
            context=context,
        )


class AllMen(ListView):
    template_name = "core/catalog_all_men.html"
    extra_context = {"title": "ALL"}
    context_object_name = "category"

    def get_queryset(self):
        queryset = Product.objects.filter(collection_name="1")

        sort_param = self.request.GET.get("sort_by")
        if sort_param == "price_a":
            queryset = queryset.order_by("price")
        elif sort_param == "price_d":
            queryset = queryset.order_by("-price")

        search_name = self.request.GET.get("search_name")
        if search_name:
            queryset = queryset.filter(name__contains=search_name)

        return queryset


class TopsMen(ListView):
    template_name = "core/catalog.html"
    extra_context = {"title": "TOPS"}
    context_object_name = "category"
    queryset = Product.objects.filter(category="1").filter(collection_name="1")


class BottomsMen(ListView):
    template_name = "core/catalog.html"
    extra_context = {"title": "BOTTOMS"}
    context_object_name = "category"
    queryset = Product.objects.filter(category="2").filter(collection_name="1")


class OuterwearMen(ListView):
    template_name = "core/catalog.html"
    extra_context = {"title": "OUTERWEAR"}
    context_object_name = "category"
    queryset = Product.objects.filter(category="3", collection_name="1")


class FootwearMen(ListView):
    template_name = "core/catalog.html"
    extra_context = {"title": "FOOTWEAR"}
    context_object_name = "category"
    queryset = Product.objects.filter(category="4").filter(collection_name="1")
