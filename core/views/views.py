from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic.detail import DetailView
from django.views.generic import CreateView
from core.models import Product
from core import forms
class Homepage(View):
    def get(self, request):
        context = {"title": "HOMEPAGE"}
        return render(
            request=request,
            template_name="core/homepage.html",
            context=context,
        )


class DetailedPage(DetailView):
    template_name = "core/detailed.html"
    model = Product
    pk_url_kwarg = "product_id"

    def get_context_data(self, **kwargs):
        product = self.object
        return super().get_context_data(title=product.name)


class CreateProduct(CreateView):
    model = Product
    fields = "__all__"
    template_name = "core/create_product.html"
    success_url = reverse_lazy("homepage")


class EditProduct(View):
    pk_url_kwarg = "product_id"

    def post_edit(request, product_id):
        product = Product.objects.get(id=product_id)
        if request.method == "POST":
            form = forms.Product(request.POST, request.FILES, instance=product)
            if form.is_valid():
                post = form.save(commit=False)
                post.save()
                return redirect('detailed_page', product.pk)
        else:
            form = forms.Product(instance=product)
        return render(request, 'core/edit_product.html', {'form': form})
